import cv2
import numpy as numpy

# Capture a video with the first web cam registered in your system
# You can load in a saved video with cv2.VideoCapture('myVideoName.avi')
cap = cv2.VideoCapture(0)
# Write the captured video using the codec XVID which should work for any OS
fourcc = cv2.VideoWriter_fourcc(*'XVID')
# cv2.VideoWriter('filename', codec, fps, size)
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))

while True:
    # if there is a return value (if ret=true) load the frame, else read
    ret, frame = cap.read()
    # convert the frame from BGR to gray
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # write the frame
    out.write(frame)

    # show color image
    cv2.imshow('frame', frame)
    # show gray image
    cv2.imshow('gray', gray)

    # if q is pressed, break the loop
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release capture and output
cap.release()
out.release()
cv2.destroyAllWindows()