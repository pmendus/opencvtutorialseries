import cv2 #Import OpenCV
import numpy as np #Import numpy. Convention is to alias numpy to 'np'
import matplotlib.pyplot as plt #Import matplotlib aliased to 'plt'

### Load image and make it grayscale ###
img = cv2.imread('test_image_doge.jpg', cv2.IMREAD_GRAYSCALE)
# imread(image_to_load, color_mode)
# color modes are IMREAD_UNCHANGED, IMREAD_GRAYSCALE, and IMREAD_COLOR
# modes can also be written as -1 for UNCHANGED, 0 for GRAYSCALE, or 1 for COLOR
# ex. img = cv2.imread('test_image.jpg', 0)

### Show image with OpenCV###
### OpenCV loads images in bgr ##
# imshow('given_image_title', loaded_image)
cv2.imshow('my_image', img)
# waitKey(delay) : a delay of 0 indicates that it will wait for a keypress forever
cv2.waitKey(0)
# close the image with the title 'my_image'
cv2.destroyWindow('my_image')
# alternatively, cv2.destroyAllWindows() can be used to close every open window

### Show image with matplotlib ###
### matplotlib shows images in rgb ###
# loading an image in color would look wrong because of the brg to rgb difference
plt.imshow(img, cmap='gray')
# plot(x_values, y_values, color, extra_params)
plt.plot([30, 100], [80, 200], 'cyan', linewidth=5)
plt.show()

### Save an image ##
# imwrite('file_name', loaded_image)
cv2.imwrite('test_save.png', img)