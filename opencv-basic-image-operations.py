import numpy as np
import cv2

img = cv2.imread('test_image_doge.jpg', cv2.IMREAD_COLOR)

# store the value of a pixel
px = img[55, 55]
# print the value of that pixel
print(px)

# you can edit the value of each pixel as well
img[55, 55] = [255, 255, 255]
px = img[55, 55]
print(px)

# take a range of pixels and save it to a region of that image
doge_face = img[100:350, 220:450]
# copy roi to new region
img[0:250, 0:230] = doge_face

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()