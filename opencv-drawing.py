import numpy as np
import cv2

img = cv2.imread('test_image_doge.jpg', cv2.IMREAD_COLOR)

# LINES
# cv2.line(image_to_draw_on, start_coordinate, end_coordinate, (b, g, r), pixel_width)
cv2.line(img, (0, 0), (150, 150), (255, 255, 255), 15)

# RECTANGLES
# cv2.rectangle(image_to_draw_on, top_left_coordinate, bottom_right, coordinate, (b, g, r), pixel_width)
cv2.rectangle(img, (250, 100), (550, 350), (255, 0, 0), 5)

# CIRCLES
# cv2.circle(image_to_draw_on, center, radius, (b, g, r), pixel_width) a negative pixel width causes fill
cv2.circle(img, (410, 190), 30, (0, 0, 255), -1)

# POLYGONS
# np.array([array], datatype)
pts = np.array([[10, 5], [20, 30], [250, 100], [70, 20], [400, 300], [50, 10]], np.int32)
# cv2.polylines(image_to_load, points_array, is_last_point_connected_to_first, (b, g, r), pixel_width)
cv2.polylines(img, [pts], True, (160, 160, 160), 3)

font = cv2.FONT_HERSHEY_SIMPLEX
# cv2.putText(image_to_write_on, string_to_write, starting_pixel, font, size_of_font, (b, g, r), thickness, anti_aliasing)
cv2.putText(img, 'OpenCV Tutorials!', (0, 400), font, 3, (0, 255, 0), 4, cv2.LINE_AA)

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()