# OpenCV Tutorial Series using Python 3.5

This tutorial series was taken from youtube sentdex. Code was written and annotated by Patrick Mendus, October 2017.

1. Install Python and set your PATH variables (if you use the python gui installer, there's an option to do that for you)
2. (Optional) Install git for windows: https://git-for-windows.github.io/
3. (Optional) Set your python path for git bash: 
    Go to your root directory: `cd ~` 
    Create a .bashrc file: `touch .bashrc`
    Open the .bashrc file: `vi .bashrc`
    Insert (`i`) `alias python='winpyt python.exe` (ESC), Save and close (`:x`)
4. Install NumPy for fundamental scientific computing: `pip install numpy`
5. Install Matplotlib for 2D plotting: `pip install matplotlib`
6. Install OpenCV for computer vision image processing: `pip install opencv-python`