import cv2
import numpy as np

img1 = cv2.imread('test_image_doge.jpg')
img2 = cv2.imread('test_image_doge2.jpg')
img3 = cv2.imread('test_image_troll.jpg')

# Look at both images in the same space. Opaqueness of both images remain the same.
add = img1 + img2
cv2.imshow('add', add)
cv2.waitKey(0)

# Add the pixel values of both images
add = cv2.add(img1, img2)
cv2.imshow('add', add)
cv2.waitKey(0)

# Add pixel values giving a weighted preference to each image
# cv2.addWeighted(image1, image1_weight, image2, image2_weight, gamma)
weighted = cv2.addWeighted(img1, 0.6, img2, 0.4, 0)
cv2.imshow('weighted', weighted)
cv2.waitKey(0)

# SUPERIMPOSE ONE IMAGE ONTO ANOTHER
# map the dimensions of an image to rows, cols, and channels
rows, cols, channels = img3.shape
# define a region of the base image as the dimensions of the imposed image
roi = img1[0:rows,0:cols]
# create a gray version of the image being imposed
img3gray = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
# return all pixels below a threshold in a clipping mask. All pixels above 220 are turned to 255 and reversed
ret, mask = cv2.threshold(img3gray, 220, 255, cv2.THRESH_BINARY_INV)
cv2.imshow('mask', mask)
cv2.waitKey(0)

# the 'invisible' parts of the mask are the black areas.
mask_inv = cv2.bitwise_not(mask)
cv2.imshow('mask_inv', mask_inv)
cv2.waitKey(0)

img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
img3_fg = cv2.bitwise_and(img3, img3, mask=mask)

dst = cv2.add(img1_bg, img3_fg)
img1[0:rows, 0:cols] = dst
cv2.imshow('res', img1)
cv2.waitKey(0)
cv2.destroyAllWindows